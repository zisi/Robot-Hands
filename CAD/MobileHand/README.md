# Physical Characteristics of Robot Finger and Hand
Robot Finger:

    DOF's: 2
	Width(mm): 16
	Lenght(mm): 70
	Weigth(g): 20 
	Force in fingertip (N): 8
	
Robot Hand:

    Actuators: 1
	Base Height(mm): 85
	Base Width(mm): 77
	Weight(g): 260
	Number of Fingers: 3

 A complete tutorial for the replication of this robot hand can be found [here.](http://www.openbionics.org/TR2015_OpenBionics_RobotHandsGuide.pdf)
